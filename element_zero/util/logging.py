import datetime

import discord


class Logger:
    def __init__(self, bot, db):
        self.bot = bot
        self.db = db

    async def _log(self, cid, **fields):
        channel = self.bot.get_channel(cid)
        if channel is None:
            pass

        footer = fields.pop('footer', None)

        e = discord.Embed(**fields)
        e.timestamp = datetime.datetime.utcnow()

        if footer:
            e.set_footer(text=footer)

        try:
            return await channel.send(embed=e)
        except discord.HTTPException:
            pass

    async def log_emoji_action(self, emoji, action, color):
        name = emoji['name']
        e = self.db.to_discord_emoji(emoji)
        owner = await self.db.find_user(emoji['owner'])
        description = f'{e} - `:{name}:`\nOwner: {owner}'

        return await self._log(392002156628017154, title=action, description=description, colour=color)

    async def log_emoji_add(self, emoji):
        return await self.log_emoji_action(emoji, 'Add', 0x4CAF50)

    async def log_emoji_remove(self, emoji):
        return await self.log_emoji_action(emoji, 'Remove', 0xF44336)

    async def log_emoji_purge(self, emoji):
        return await self.log_emoji_action(emoji, 'Purge', 0xFF9800)

    async def log_emoji_decay(self, emoji):
        await self.log_emoji_action(emoji, 'Decay', 0x607D8B)
